import UIKit

enum NotePriority: Int {
    case low = 0
    case normal
    case high
}

struct Note {
    let uid: String
    let title: String
    let content: String
    let color: UIColor
    let priority: NotePriority
    let dateOfSelfDestruction: Date?
    
    init(title: String, content: String, priority: NotePriority, uid: String = UUID().uuidString, color: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), dateOfSelfDestruction: Date? = nil) {
        
        self.title = title
        self.content = content
        self.uid = uid
        self.priority = priority
        self.color = color
        self.dateOfSelfDestruction = dateOfSelfDestruction
        
    }
}
