import UIKit

extension Note {
    static func parse(json: [String: Any]) -> Note? {
        guard json["uid"] != nil else {
            return nil
        }
        guard json["title"] != nil else {
            return nil
        }
        guard json["content"] != nil else {
            return nil
        }
        
        let uid = json["uid"] as! String
        let title = json["title"] as! String
        let content = json["content"] as! String
        var priority = NotePriority.normal
        if json["priority"] != nil {
            priority = NotePriority(rawValue: json["priority"] as! Int) ?? priority
        }
        
        var dateOfSelfDestruction: Date? = nil
        if json["dateOfSelfDestruction"] != nil {
            dateOfSelfDestruction = Date(timeIntervalSince1970: json["dateOfSelfDestruction"] as! Double)
        }
        
        var color = UIColor.white
        if json["color"] != nil {
            let rgba = json["color"] as! Array<CGFloat>
            
            color = UIColor(red: rgba[0], green: rgba[1], blue: rgba[2], alpha: rgba[3])
        }
        
        
        return Note(title: title, content: content, priority: priority, uid: uid, color: color, dateOfSelfDestruction: dateOfSelfDestruction)
        
    }
    
    var json: [String: Any] {
        var dict = [String: Any]()
        dict["uid"] = self.uid
        dict["title"] = self.title
        dict["content"] = self.content
        if self.color != UIColor.white {
            var r: CGFloat = 0
            var g: CGFloat = 0
            var b: CGFloat = 0
            var a: CGFloat = 0
            self.color.getRed(&r, green: &g, blue: &b, alpha: &a)
            dict["color"] = [r,g,b,a]
        }
        if self.priority != NotePriority.normal {
            dict["priority"] = self.priority.rawValue
        }
        if self.dateOfSelfDestruction != nil {
            dict["dateOfSelfDestruction"] = self.dateOfSelfDestruction?.timeIntervalSince1970
        }
        return dict
    }
    
    
}

