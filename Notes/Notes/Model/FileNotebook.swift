import UIKit

class FileNotebook {
    
    public private(set) var notes: [Note] = []
    
    public func add(_ note: Note){
        notes.append(note)
    }
    
    public func remove(with uid: String) {
        notes.removeAll{ $0.uid == uid }
    }
    
    public func add(_ note: Note, with uid: String) {
        for (index, item) in notes.enumerated() {
            if item.uid == uid {
                notes[index] = note
            }
        }
    }
    
    public func destroyNotesByDate() -> Bool {
        var isDestroyed = false
        
        for (index, item) in notes.enumerated() {
            if let date = item.dateOfSelfDestruction {
                
                notes.remove(at: index)
                
                isDestroyed = true
            }
        }
        
        saveToFile()
        
        return isDestroyed
    }
    
    public func saveToFile() {
        
        let path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        guard path != nil else {
            return
        }
        
        let dir = path!.appendingPathComponent("Notebooks", isDirectory: true)
        var isDir: ObjCBool = false
        
        if !FileManager.default.fileExists(atPath: dir.path, isDirectory: &isDir), !isDir.boolValue {
            do {
                try FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true,
                                                        attributes: [:])
            } catch { }
        }
        let filename = dir.appendingPathComponent("FileNotebook.data")
        do {
            let data = try JSONSerialization.data(withJSONObject: notes.map { $0.json }, options: [])
            //FileManager.default.createFile(atPath: filename.path, contents: data, attributes: [:])
            try data.write(to: filename)
        } catch { }
    }
    
    public func loadFromFile() {
        var path = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        guard path != nil else {
            return
        }
        notes.removeAll()
        
        path = path!.appendingPathComponent("Notebooks", isDirectory: true)
        path = path!.appendingPathComponent("FileNotebook.data", isDirectory: false)
        
        if FileManager.default.fileExists(atPath: path!.path) {
            do {
                let data = try Data.init(contentsOf: path!)
                let items = try JSONSerialization.jsonObject(with: data, options: []) as! Array<[String: Any]>
                for i in items {
                    let note = Note.parse(json: i)
                    if note != nil {
                        notes.append(note!)
                    }
                }
            } catch { }
        }
    }
}
