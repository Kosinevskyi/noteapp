//
//  DrawFlag.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/11/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

@IBDesignable
class DrawFlag: UIView {

    @IBInspectable var flagColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable var isFlagHidden: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable var isPaletteActive: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }

    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if isPaletteActive {
            let col = ColorPickerPaletteView()
            col.draw(rect)
        }
        
        if !isFlagHidden {
            self.layer.sublayers?.removeAll()
            return
        }

        drawRingFittingInsideView()
    }
    
    func drawRingFittingInsideView() {
        let circleRadius: CGFloat = 15
        let offset: CGFloat = 5
        let lineWidth: CGFloat = 3.0
        
        let xBoundsCircle: CGFloat = bounds.maxX - circleRadius - offset
        let yBoundsCircle: CGFloat = bounds.minY + circleRadius + offset
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: xBoundsCircle,y: yBoundsCircle), radius: CGFloat(circleRadius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        
        let shapeLayer2 = CAShapeLayer()
        shapeLayer2.path = circlePath.cgPath
        shapeLayer2.fillColor = UIColor.clear.cgColor
        shapeLayer2.strokeColor = UIColor.white.cgColor
        shapeLayer2.lineWidth = lineWidth + 2
        self.layer.addSublayer(shapeLayer2)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = flagColor.cgColor
        shapeLayer.lineWidth = lineWidth
        self.layer.addSublayer(shapeLayer)

        let topLineBackground = UIBezierPath()
        topLineBackground.move(to: CGPoint(x: bounds.maxX - circleRadius, y: bounds.minY + lineWidth + offset))
        topLineBackground.addLine(to: CGPoint(x: bounds.maxX - offset - circleRadius, y: bounds.minY + circleRadius * 2))
        topLineBackground.addLine(to: CGPoint(x: bounds.maxX - circleRadius * 2, y: bounds.minY + lineWidth + offset + circleRadius))
        
        let topLineCABackground = CAShapeLayer()
        topLineCABackground.path = topLineBackground.cgPath
        topLineCABackground.fillColor = UIColor.clear.cgColor
        topLineCABackground.strokeColor = UIColor.white.cgColor
        topLineCABackground.lineWidth = lineWidth + 2
        self.layer.addSublayer(topLineCABackground)
        
        let topLineCA = CAShapeLayer()
        topLineCA.path = topLineBackground.cgPath
        topLineCA.fillColor = UIColor.clear.cgColor
        topLineCA.strokeColor = UIColor.black.cgColor
        topLineCA.lineWidth = lineWidth
        self.layer.addSublayer(topLineCA)
    }
}
