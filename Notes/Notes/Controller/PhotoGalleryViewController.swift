//
//  PhotoGalleryViewController.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/18/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit


/*
 var images = [UIImage]()
 
 @IBOutlet var imageCollectionView: UICollectionView!
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(importPicture))
 
 initPhotos()
 }
 
 private func initPhotos(){
 for i in 1...5 {
 if let img = UIImage(named: "photo_\(i)") {
 images.append(img)
 }
 }
 }
 */

class PhotoGalleryViewController: UICollectionViewController {

    var images = [UIImage]()
    
    @IBOutlet var imageCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initPhotos()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpUI()
    }
    
    private func setUpUI() {
        navigationController?.topViewController?.title = "Галерея"
        navigationController?.topViewController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(importPicture))
        
        navigationController?.topViewController?.navigationItem.leftBarButtonItem = nil
    }
    
    private func initPhotos(){
        for i in 1...5 {
            if let img = UIImage(named: "photo_\(i)") {
                images.append(img)
            }
        }
    }
}

extension PhotoGalleryViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageView", for: indexPath)
        
        if let imageView = cell.viewWithTag(1000) as? UIImageView {
            imageView.image = images[indexPath.item]
        }
        
        return cell
    }
    
//    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool{
//        let cell = collectionView.cellForItem(at: indexPath)
//        self.performSegue(withIdentifier: "showImage", sender: cell)
//        return true
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showImage" {
            if let cell = sender as? UICollectionViewCell,
            let indexPath = imageCollectionView.indexPath(for: cell) {
                let galleryController = segue.destination as! GalleryViewController
                imageCollectionView.deselectItem(at: indexPath, animated: true)
                galleryController.images = images
                galleryController.currentPage = indexPath.row
            }
        }
    }
    
    @objc func importPicture() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        
        dismiss(animated: true)
        
        images.insert(image, at: 0)
        collectionView.reloadData()
    }
}
