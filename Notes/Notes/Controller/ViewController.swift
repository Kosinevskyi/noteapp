//
//  ViewController.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/1/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

internal protocol NoteChangedDelegate {
    func createdNewNote(note: Note)
    func updatedNote(note: Note)
}

class ViewController: UIViewController {
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var noteField: UITextView!
    @IBOutlet var colorViews: [UIView]!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var colorPickerController: ColorPickerView!
    @IBOutlet weak var colorPickerView: UIView!

    @IBOutlet weak var destroyDateSwitch: UISwitch!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var lastSelectedColor: UIColor = .white
    
    
    internal var delegate: NoteChangedDelegate?

    var note: Note? = nil
    private var lastSelectedUID: String?
    
    @IBAction func useDestroyDateSwitchAction(_ sender: UISwitch) {
        if sender.isOn {
            datePickerView.isHidden = false
        } else {
            datePickerView.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateAndSubscribeUIsOnStart()
        
        colorPickerController.doneButtonHandler = { [weak self] in
            self?.hideUnhideViews()
        }
        
        if note != nil {
            updateNoteInfo(note: note!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if note == nil {
            self.delegate?.createdNewNote(note: configurateNoteForSave())
        } else {
            self.delegate?.updatedNote(note: configurateNoteForSave())
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation){
        colorPickerController.deviceChangeRotation()
    }
    
    private func hideUnhideViews() {
        
        colorViews[colorViews.count - 1].backgroundColor = colorPickerController.selectedColorView.backgroundColor
        
        lastSelectedColor = colorPickerController.selectedColorView.backgroundColor!
        
        scrollView.isHidden = colorPickerView.isHidden
        colorPickerView.isHidden = !colorPickerView.isHidden
    }

    private func updateAndSubscribeUIsOnStart() {
        noteField.layer.cornerRadius = 5
        let elementsCount = colorViews.count

        for (index, view) in colorViews.enumerated() {
            view.layer.borderColor = UIColor.black.cgColor
            view.layer.borderWidth = 1
            
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.selectColor(_:)))
            let tap2 = UILongPressGestureRecognizer(target: self, action: #selector(self.selectColorPicker(_:)))
            
            view.addGestureRecognizer(index == elementsCount - 1 ? tap2 : tap1)
        }

        changeViewFlag(view: colorViews[0] as! DrawFlag, isActive: true)
    }
    
    @objc func selectColor(_ sender: UITapGestureRecognizer) {
        setInactiveDisplayForViews()
        
        changeViewFlag(view: sender.view as! DrawFlag, isActive: true)
    }
    
    private func selectColor1(_ sender: UILongPressGestureRecognizer) {
        setInactiveDisplayForViews()
        
        changeViewFlag(view: sender.view as! DrawFlag, isActive: true)
    }

    @objc func selectColorPicker(_ sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            selectColor1(sender)
            
            hideUnhideViews()
            
            let drawFlag = sender.view as! DrawFlag
            drawFlag.isPaletteActive = false
        }
    }
    
    private func setInactiveDisplayForViews() {
        for view in colorViews {
            changeViewFlag(view: view as! DrawFlag)
        }
    }
    
    private func changeViewFlag(view: DrawFlag, isActive: Bool = false) {
        view.isFlagHidden = isActive
        
        if isActive {
            let view = view as UIView
            lastSelectedColor = view.backgroundColor ?? .white
        }
    }
}


extension ViewController {
    private func updateNoteInfo(note: Note) {
        
        titleField.text = note.title
        noteField.text = note.content
        selectRightColorPanel(color: note.color)
        lastSelectedColor = note.color
        lastSelectedUID = note.uid
        
        if note.dateOfSelfDestruction != nil {
            destroyDateSwitch.isOn = true
            useDestroyDateSwitchAction(destroyDateSwitch)
            datePicker.date = note.dateOfSelfDestruction!
        }
    }
    
    private func selectRightColorPanel(color: UIColor) {
        var selectedIndex = 0
        
        for (index, colorView) in colorViews.enumerated() {
            if index < colorViews.count - 1 {
                if colorView.backgroundColor!.isEqual(color) {
                    selectedIndex = index
                    break
                }
            } else {
                selectedIndex = index
                break
            }
        }
        
        setInactiveDisplayForViews()
        changeViewFlag(view: colorViews[selectedIndex] as! DrawFlag, isActive: true)
        
        let drawFlag = colorViews[selectedIndex] as! DrawFlag
        drawFlag.isPaletteActive = false
        
        colorViews[selectedIndex].backgroundColor = color
    }
    
    private func configurateNoteForSave() -> Note {
        let title = titleField.text ?? ""
        let content = noteField.text ?? ""
        
        let dateOfSelfDestruction: Date? = datePickerView.isHidden ? nil : datePicker.date

        return lastSelectedUID == nil ? Note(title: title, content: content, priority: .low, color: lastSelectedColor, dateOfSelfDestruction: dateOfSelfDestruction) : Note(title: title, content: content, priority: .low, uid: lastSelectedUID!, color: lastSelectedColor, dateOfSelfDestruction: dateOfSelfDestruction)
    }
}
