//
//  NotesListViewController.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/18/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class NotesListViewController: UIViewController {

    @IBOutlet weak var itemsTableView: UITableView!
    
    let fileNotebook = FileNotebook()
    
    private var delegate: NoteChangedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "NoteListTableViewCell", bundle: nil)
        
        itemsTableView.register(nib, forCellReuseIdentifier: "default")
        
        fileNotebook.loadFromFile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setUpUI()
        
        fileNotebook.loadFromFile()
        
        destroyNotesByDate()
    }

    @objc private func addTapped() {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewControllerID") as? ViewController

        self.navigationController?.pushViewController(vc!, animated: true)

        vc?.delegate = self
    }
    
    @objc private func editTapped() {
        
        if itemsTableView.numberOfRows(inSection: 0) > 0 {
            itemsTableView.isEditing = !itemsTableView.isEditing
            navigationController?.topViewController?.navigationItem.leftBarButtonItem?.tintColor = itemsTableView.isEditing ? .red : UIColor(red: 0, green: 0.478431, blue: 1, alpha: 1)
        }
    }
    
    private func setUpUI() {
        navigationController?.topViewController?.title = "Заметки"
        navigationController?.topViewController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        navigationController?.topViewController?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editTapped))
    }

    private func checkDataForCorrect(note: Note) -> Bool {
        if !note.title.isEmpty && !note.content.isEmpty {
            return true
        }
        return false
    }
    
    private func destroyNotesByDate() {
        if fileNotebook.destroyNotesByDate() {
            itemsTableView.reloadData()
        }
    }
}

extension NotesListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return fileNotebook.notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "default", for: indexPath) as! NoteListTableViewCell
        
        let note = fileNotebook.notes[indexPath.row]
        
        cell.noteTitle.text = note.title
        cell.noteContent.text = note.content
        cell.noteColor.backgroundColor = note.color
        cell.uid = note.uid
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewControllerID") as? ViewController
        
        vc?.note = fileNotebook.notes[indexPath.row]
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
        vc?.delegate = self
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            fileNotebook.remove(with: fileNotebook.notes[indexPath.row].uid)
            
            fileNotebook.saveToFile()
            
            itemsTableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

extension NotesListViewController: NoteChangedDelegate {
    func createdNewNote(note: Note) {

        if !checkDataForCorrect(note: note) {
            print("Bad data!!!")
            return
        }
        
        fileNotebook.add(note)

        fileNotebook.saveToFile()
        
        itemsTableView.reloadData()
    }
    
    func updatedNote(note: Note) {
        
        if !checkDataForCorrect(note: note) {
            print("Bad data!!!")
            return
        }

        fileNotebook.add(note, with: note.uid)
        
        fileNotebook.saveToFile()
        
        itemsTableView.reloadData()
    }
}
