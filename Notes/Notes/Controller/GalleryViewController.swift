//
//  GalleryViewController.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/25/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    @IBOutlet var scrollView: UIScrollView!
    
    var lastOpenedPage: Int?
    var currentPage: Int = 0
    var imageViews = [UIImageView]()
    var images = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        for image in images {
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            
            scrollView.addSubview(imageView)
            imageViews.append(imageView)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for (index, imageView) in imageViews.enumerated() {
            imageView.frame.size = scrollView.frame.size
            imageView.frame.origin.x = scrollView.frame.width * CGFloat(index)
            imageView.frame.origin.y = 0
        }
        
        let contentWidth = scrollView.frame.width * CGFloat(imageViews.count)
        scrollView.contentSize = CGSize(width: contentWidth,
                                        height: scrollView.frame.height)
        
        if let lastOpenedPage = lastOpenedPage {
            currentPage = lastOpenedPage
        }
        
        // Выравниваем изображение при поворотах экрана
        scrollView.contentOffset.x = imageViews[currentPage].frame.minX
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        lastOpenedPage = currentPage
    }
}

extension GalleryViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = (scrollView.contentOffset.x / scrollView.frame.width)
        currentPage = Int(round(Double(pageIndex)))
    }

}
