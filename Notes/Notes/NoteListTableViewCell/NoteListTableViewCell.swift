//
//  NoteListTableViewCell.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/20/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class NoteListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noteColor: UIView!
    @IBOutlet weak var noteTitle: UILabel!
    
    @IBOutlet weak var noteContent: UILabel!
    
    public var uid: String = ""
}
