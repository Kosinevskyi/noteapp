//
//  ColorPickerViewClass.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/12/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

@IBDesignable
class ColorPickerView: UIView {
    @IBOutlet weak var selectedColorView: UIView!
    @IBOutlet weak var hexSelectedColorView: UIView!
    @IBOutlet weak var gradientColorPickerView: ColorPickerPaletteView!

    @IBOutlet weak var hexColorLabel: UILabel!
    
    var doneButtonHandler: (() -> Void)?
    
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBAction func brightnessChangedAction(_ sender: UISlider) {
        gradientColorPickerView.changeBrightness(brightness: CGFloat(sender.value))
    }
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        doneButtonHandler?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func deviceChangeRotation() {
        gradientColorPickerView.drawSight(point: gradientColorPickerView.getPointForColor(color: selectedColorView.backgroundColor ?? .black))
    }
    
    private func setupViews() {
        let xibView = loadViewFromXib()
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(xibView)
        
        updateUI()
        gradientColorPickerView.delegate = self
    }
    
    private func updateUI() {
        selectedColorView.layer.borderColor = UIColor.black.cgColor
        selectedColorView.layer.borderWidth = 1
        selectedColorView.layer.cornerRadius = 10
        selectedColorView.layer.masksToBounds = true
        
        hexSelectedColorView.layer.borderColor = UIColor.black.cgColor
        hexSelectedColorView.layer.borderWidth = 1
        
        gradientColorPickerView.layer.borderColor = UIColor.black.cgColor
        gradientColorPickerView.layer.borderWidth = 1
    }
    
    private func loadViewFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ColorPickerView", bundle: bundle)
        
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
    
}

extension ColorPickerView: HSBColorPickerDelegate {
    func HSBColorColorPickerTouched(sender: ColorPickerPaletteView, color: UIColor, point: CGPoint, hexColor: String) {
        
        selectedColorView.backgroundColor = color
        hexColorLabel.text = hexColor
        brightnessSlider.value = Float(sender.brightnessValue)
    }
}
