//
//  ColorPickerView.swift
//  Notes
//
//  Created by Roman Kosinevskyi on 7/11/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit


internal protocol HSBColorPickerDelegate : NSObjectProtocol {
    func HSBColorColorPickerTouched(sender:ColorPickerPaletteView, color:UIColor, point:CGPoint, hexColor: String)
}

@IBDesignable
class ColorPickerPaletteView : UIView {

    private enum Sides {case left, right, top, bottom}
    
    weak internal var delegate: HSBColorPickerDelegate?
    let saturationExponentTop:Float = 0.1
    let saturationExponentBottom:Float = 1.0
    
    var brightnessValue: CGFloat = .zero
    var lastPoint: CGPoint = .zero
    
    
    @IBInspectable var elementSize: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private func initialize() {
        self.clipsToBounds = true
        let touchGesture = UIPanGestureRecognizer(target: self, action: #selector(self.touchedColor(gestureRecognizer:)))//UILongPressGestureRecognizer(target: self, action: #selector(self.touchedColor(gestureRecognizer:)))
        //touchGesture.minimumPressDuration = 0
        //touchGesture.allowableMovement = CGFloat.greatestFiniteMagnitude
        self.addGestureRecognizer(touchGesture)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }

    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        for y : CGFloat in stride(from: 0.0 ,to: rect.height, by: elementSize) {
            var saturation = y < rect.height / 2.0 ? CGFloat(2 * y) / rect.height : 2.0 * CGFloat(rect.height - y) / rect.height
            saturation = CGFloat(powf(Float(saturation), y < rect.height / 2.0 ? saturationExponentTop : saturationExponentBottom))
            let brightness = y < rect.height / 2.0 ? CGFloat(1.0) : 2.0 * CGFloat(rect.height - y) / rect.height
            for x : CGFloat in stride(from: 0.0 ,to: rect.width, by: elementSize) {
                let hue = x / rect.width
                let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
                context!.setFillColor(color.cgColor)
                context!.fill(CGRect(x:x, y:y, width:elementSize,height:elementSize))
            }
        }
        
        lastPoint = CGPoint(x: bounds.maxX / 2, y: bounds.maxY / 2)
        calculateColor(point: lastPoint)
    }
    
    func getColorAtPoint(point:CGPoint) -> UIColor {
        let roundedPoint = CGPoint(x:elementSize * CGFloat(Int(point.x / elementSize)),
                                   y:elementSize * CGFloat(Int(point.y / elementSize)))
        var saturation = roundedPoint.y < self.bounds.height / 2.0 ? CGFloat(2 * roundedPoint.y) / self.bounds.height
            : 2.0 * CGFloat(self.bounds.height - roundedPoint.y) / self.bounds.height
        saturation = CGFloat(powf(Float(saturation), roundedPoint.y < self.bounds.height / 2.0 ? saturationExponentTop : saturationExponentBottom))
        let brightness: CGFloat = roundedPoint.y < self.bounds.height / 2.0 ? CGFloat(1.0) : 2.0 * CGFloat(self.bounds.height - roundedPoint.y) / self.bounds.height
        
        let hue = roundedPoint.x / self.bounds.width
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }

    func getPointForColor(color:UIColor) -> CGPoint {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil);
        
        var yPos:CGFloat = 0
        let halfHeight = (self.bounds.height / 2)
        if (brightness >= 0.99) {
            let percentageY = powf(Float(saturation), 1.0 / saturationExponentTop)
            yPos = CGFloat(percentageY) * halfHeight
        } else {
            //use brightness to get Y
            yPos = halfHeight + halfHeight * (1.0 - brightness)
        }
        var xPos = hue * self.bounds.width
        
        if color.toHexString() == "#ffffff" || color.toHexString() == "#000000"{
            xPos = lastPoint.x
        }
        lastPoint = CGPoint(x: xPos, y: yPos)
        return lastPoint
    }

    @objc func touchedColor(gestureRecognizer: UIPanGestureRecognizer) {
        //if (gestureRecognizer.state == UIGestureRecognizer.State.began) {
            lastPoint = gestureRecognizer.location(in: self)
            calculateColor(point: lastPoint)
        //}
    }
    
    private func calculateColor(point: CGPoint) {
        //let point =
        let color = getColorAtPoint(point: point)
        brightnessValue = 1 - ((point.y * 100) / bounds.maxY) / 100
        
        drawSight(point: point)
        
        self.delegate?.HSBColorColorPickerTouched(sender: self, color: color, point: point, hexColor: color.toHexString())
    }
    
    func drawSight(point: CGPoint) {
        self.layer.sublayers?.removeAll()
        
        let circleRadius: CGFloat = 10
        let lineWidth: CGFloat = 1.0

        let circlePath = UIBezierPath(arcCenter: point, radius: CGFloat(circleRadius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        let circuit = CAShapeLayer()
        circuit.path = circlePath.cgPath
        circuit.fillColor = UIColor.clear.cgColor
        circuit.strokeColor = UIColor.white.cgColor
        circuit.lineWidth = lineWidth + 2
        self.layer.addSublayer(circuit)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.black.cgColor
        shapeLayer.lineWidth = lineWidth
        self.layer.addSublayer(shapeLayer)

        drawLine(center: point, side: .top, height: circleRadius, lineWidth: lineWidth)
        drawLine(center: point, side: .bottom, height: circleRadius, lineWidth: lineWidth)
        drawLine(center: point, side: .left, height: circleRadius, lineWidth: lineWidth)
        drawLine(center: point, side: .right, height: circleRadius, lineWidth: lineWidth)
    }
    
    private func drawLine(center point: CGPoint, side: Sides, height: CGFloat, lineWidth: CGFloat) {
        var firstPoint: CGPoint = .zero
        var seconfPoint: CGPoint = .zero
        
        switch side {
        case .top:
            firstPoint = CGPoint(x: point.x, y: point.y - height)
            seconfPoint = CGPoint(x: point.x, y: point.y - height * 2)
        case .bottom:
            firstPoint = CGPoint(x: point.x, y: point.y + height)
            seconfPoint = CGPoint(x: point.x, y: point.y + height * 2)
        case .left:
            firstPoint = CGPoint(x: point.x - height, y: point.y)
            seconfPoint = CGPoint(x: point.x - height * 2, y: point.y)
        case .right:
            firstPoint = CGPoint(x: point.x + height, y: point.y)
            seconfPoint = CGPoint(x: point.x + height * 2, y: point.y)
        }
        
        let topLineBackground = UIBezierPath()
        topLineBackground.move(to: firstPoint)
        topLineBackground.addLine(to: seconfPoint)
        
        let topLineCABackground = CAShapeLayer()
        topLineCABackground.path = topLineBackground.cgPath
        topLineCABackground.strokeColor = UIColor.white.cgColor
        topLineCABackground.lineWidth = lineWidth + 2
        self.layer.addSublayer(topLineCABackground)
        
        let topLineCA = CAShapeLayer()
        topLineCA.path = topLineBackground.cgPath
        topLineCA.strokeColor = UIColor.black.cgColor
        topLineCA.lineWidth = lineWidth
        self.layer.addSublayer(topLineCA)
    }
    
    func changeBrightness(brightness: CGFloat) {
//        var point = getPointForColor(color: color)
//        var height = (bounds.height * brightness * 100) / 100
//
//        height = bounds.maxY - height
//
//        point.y = height
        
        var point = lastPoint
        
        let height = bounds.maxY - ((bounds.maxY * brightness * 100) / 100)
        point.y = height
        //brightnessValue
        
        calculateColor(point: point)
        //drawSight(point: point)

    }
}

extension UIColor {
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
}
